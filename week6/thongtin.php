<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Đăng ký Tân sinh viên</title>

    <style>
        .establish {
        display: flex;
        
        line-height: 30px;
        margin-top: 13px;
        }
        .box_label {
            width: 140px;
            max-width: 130px;
             background-color: rgb(102, 143, 255);
            margin-right: 40px;
            text-align: center;
            color: #fff;border: 2px solid #0b67ad;
        }
         .box_label1 {
            margin-top: 0px;
            margin-bottom: 100px;
            width: 140px;
            max-width: 130px;
             background-color: rgb(102, 143, 255);
            margin-right: 30px;
            text-align: center;
            color: #fff;border: 2px solid #0b67ad;
        }
        .infor_text {
            /*margin-top: 18px;*/
            width: 600px;
    }
      
        .signup {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 1.2rem 2rem;
        }

        

       

        .input-text {
            width: 20rem;
            height: 2.6rem;
            padding-top: 2rem;
        }

        .input-text-img {
            
        }

         .signup-submit {
        display: flex;
        justify-content: center;
        margin-top: 2rem;

    }

      input[type="submit"] {
        margin-top: 0px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }
    </style>
</head>

<body>
    
    
        <div class="signup"  style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 50px 40px 50px 50px;
        position: absolute;
        margin-left: 20rem;
        margin-top: 2%;
        width: 48rem;
    ">
            <?php
                session_start();
                $fullName = isset($_SESSION['fullName']) ? $_SESSION['fullName'] : "";
                $gender = isset($_SESSION['gender']) ? $_SESSION['gender'] : "";
                $department = isset($_SESSION['department']) ? $_SESSION['department'] : "";
                $ngaysinh = isset($_SESSION['ngaysinh']) ? $_SESSION['ngaysinh'] : "";
                $address = isset($_SESSION['address']) ? $_SESSION['address'] : "";
                $image = isset($_SESSION['image']) ? $_SESSION['image'] : "";
            ?>
 
            <form method="POST" id="form" enctype="multipart/form-data">
                <div class="establish">
                    <label class="box_label" style="flex: 1">
                        Họ và tên
                        <span style="color: red">*</span>
                    </label>

                    <div class="infor_text"> <?php echo $fullName?> </div>
                </div>
                <div class="establish">
                   <label class="box_label" style="flex: 1">
                        Giới tính
                        <span style="color: red">*</span>
                    </label>

                    <div class="infor_text"> <?php echo $gender?> </div>

                </div>
                <div class="establish">
                     <label class="box_label" style="flex: 1">
                    
                        Phân khoa
                        <span style="color: red">*</span>

                    </label>
                    <div class="infor_text"> <?php echo $department?> </div>
                </div>
                <div class="establish">
                   <label class="box_label" style="flex: 1">
                    
                        Ngày sinh
                        <span style="color: red">*</span>

                    </label>

                    <div class="infor_text"> <?php echo $ngaysinh?>


                    </div>
                </div>
                <div class="establish">
                     <label class="box_label" style="flex: 1">
                        Địa chỉ
                        <span style="color: red">*</span>

                    </label>
                    <div class="infor_text"> <?php echo $address?>
                    </div>
                </div>


                <div class="establish">
                    <p class="box_label1">
                        Hình ảnh
                    </p>
                    <div class="input-text-img">
                        <?php
                            echo '<span ><img src="' . $image . '" height="120px" width="120px"></span>'
                        ?>
                    </div>
                </div>

                <div class="establish signup-submit">
                    <input type="submit" value="Xác nhận">
                </div>
            </form>
        </div>

   


</body>

</html>