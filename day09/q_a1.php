<?php 
    if(!empty($_COOKIE)) { 
        $errors  = array();
        $values  = array();
        $date = new DateTime();
        $new_date = $date->format('d/m/Y');
        $flag = true;
        $data = json_decode($_COOKIE['arr_answer'], true);    
        if(!empty($_POST)) {
            $question6 = $_POST['question6'];
            $question7 = $_POST['question7'];
            $question8 = $_POST['question8'];
            $question9 = $_POST['question9'];
            $question10 = $_POST['question10'];
            $arr_answer2 = array($question6,$question7,$question8,$question9,$question10);
            $list_answer = array_merge($data,$arr_answer2);
            setcookie('list_answer',json_encode($list_answer),time() + (86400 * 30), "/" );
            header('Location: finish_exam.php');
            die();
    }

    if(count($_COOKIE) > 0) {
  echo "Cookies chưa tạo.";
} else {
  echo "Cookies đã tạo.";
}
}
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>

    <style>
        .establish {
         display: flex;
        Alight-item:center;
        line-height: 30px;
        margin-top: 8px;
        }
        
         
        .infor_text {
            /*margin-top: 18px;*/
            width: 600px;
    }
         .space {
            width: 8rem;
        }
        .box_label {
        width: 140px;
        max-width: 130px;
         background-color: rgb(20, 143, 255);
        margin-right: 30px;
        font-size: 15px;
         justify-content: center;
        color: #fff;border: 2px solid #0b67ad;
    }
       
         .signup-submit {
        display: flex;
        justify-content: center;
        margin-top: 2rem;
        margin-right: 8.5rem;
        margin-bottom: 2rem;
    }
     .signup-submit1 {
        display: flex;
        justify-content: right;
        margin-top: 2rem;
        margin-right: 5rem;

    }
    .add_remove {
        display: flex;
        justify-content: right;
        margin-top: 2rem;
    }
    .input-text {
        width: 15rem;
        /*height: 2.25rem;*/
        padding-left: 0.5rem;
        border: 2px solid #4e7aa3;
    }
        .input-select {
        width: 16rem;
        /*height: 2.25rem;*/
        padding-left: 0.5rem;
        border: 2px solid #4e7aa3;
    }
      input[type="submit"] {
        margin-top: 0px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }
    .submit_delete{
        margin-top: 0px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }.submit_search{
        margin-top: 0px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }
    .action {
        height: 25px; 
        width: 50px; 
        border-radius: 6px; 
        border: 2px solid rgb(100, 51, 255); 
        background-color: rgb(75, 10, 204); 
        color: #fff; 
        font-size: 15px;
    }
    .space{
        width: 10rem;
    }
    .title{
        text-align: left;
    }
    .search{}
    table, th, td {
  border: 1px solid black;
}

.quiz,
.choices {
    list-style-type: none;
    padding: 0;
}
.choices li {
    margin-bottom: 5px;
}

.choices label,
input[type="radio"] {
    cursor: pointer;
}
input[type="radio"] {
    margin-right: 8px;
}

    </style>
</head>
<body>
    
     <form method="post">  

        <div  style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 60px 0px 50px 50px;
        position: absolute;
        margin-left: 20rem;
        margin-top: 2%;
        width: 45rem;
    "><form method="post">
                
                
               
   <ul class="quiz">
    <li>
        <h4>6. Loài nào dưới đây thuộc nhóm "Động vật máu nóng"?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question6" value="A" /><span
                         di>Rồng </span 
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question6" value="B" /><span
                        >CÓc</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question6" value="C" /><span
                        >Cá sấu</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question6" value="D" /><span
                        >Chuột</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>7. Đâu không phải là một ngôn ngữ lập trình?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question7" value="A" /><span
                        >python</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question7" value="B" /><span
                        >JavaScript</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question7" value="C" /><span
                        >Pascal</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question7" value="D" /><span
                        >Hyperlink</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>8.Cuộc gọi giữa nhà phát minh Alexander Graham Bell và...?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question8" value="A" /><span
                        >Vợ của ông</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question8" value="B" /><span
                        >Và cái quần </span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question8" value="C" /><span
                        >vục x__i__t</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question8" value="D" /><span
                        >Người trợ lý ảo</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>9. Việt Nam mấy lần vô địch AFF cup?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question9" value="A" /><span
                        >1</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question9" value="B" /><span
                        >2</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question9" value="C" /><span
                        >5</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question9" value="D" /><span
                        >4</span
                    ></label
                >
            </li>
        </ul>
    </li>
    <li>
        <h4>10.Việt Nam có bao nhiêu dân tộc?</h4>
        <ul class="choices">
            <li>
                <label
                    ><input type="radio" name="question10" value="A" /><span
                        >21</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question10" value="B" /><span
                        >44</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question10" value="C" /><span
                        >55</span
                    ></label
                >
            </li>
            <li>
                <label
                    ><input type="radio" name="question10" value="D" /><span
                        >54</span
                    ></label
                >
            </li>
        </ul>
    </li>
    </ul>
 <button  class="establish box_label " style="margin-left: 80%;">Next</button>
    

    
            </div>
 </form>
   
</body>



</html>