<?php
$gender = array('0' => 'Nam', '1' => 'Nữ');
$phankhoa = array("EMPTY"=>"", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
date_default_timezone_set('Asia/Calcutta');
$date = new DateTime();
$new_date = $date->format('d/m/Y');
$flag = true;   
$fileupload = '';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Đăng ký Tân sinh viên</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
  

    <style>
    
    .establish {
        display: flex;
        Alight-item:center;
        line-height: 30px;
        margin-top: 8px;
    }
     .box_label {
        width: 140px;
        max-width: 130px;
         background-color: rgb(102, 143, 255);
        margin-right: 30px;
        color: #fff;border: 2px solid #0b67ad;
    }



    .input-text {
        width: 30rem;
        height: 2.25rem;
        padding-left: 0.5rem;
        border: 2px solid #4e7aa3;
    }

    .gender {
        display: flex;
        align-items: center;
        margin-left: 0.5rem;
    }

    select {
        border: 2px solid #4e7aa3;
        padding: 0px;
        outline: none;
        width: 30%;
        height: 2.6rem;
    }

    .input-ngaysinh {
        width: 30%;
        height: 2.3rem;
        padding-left: 0.4rem;
        border: 2px solid #4e7aa3;
    }

    input[type="file"] {
        width: 22rem;
        height: 2.3rem;
        
    }

    .signup-submit {
        display: flex;
        justify-content: center;
        margin-top: 2rem;

    }

    input[type="submit"] {
        margin-top: 20px;
        height: 50px; 
        width: 120px; 
        border-radius: 6px; 
        border: 2px solid rgb(51, 51, 255); 
        background-color: rgb(0, 0, 204); 
        color: #fff; 
        font-size: 15px;
    }
    </style>
</head>

<body>
<center>
    <fieldset>
         <?php
        if (!empty($_POST['btnSubmit'])) {
            session_start();
 
            //get name image
         
            $error = 1;
          
                    
            if (empty(($_POST["fullName"]))){
                echo "<div style='color: red;'>Hãy nhập tên.</div>";
                $error = 0;
            }
             else {                  
                $_SESSION['fullName'] = $_POST['fullName'];
            }
            

             if (empty(($_POST["address"]))){
                echo "<div style='color: red;'>Hãy nhập địa chỉ.</div>";
                $error = 0;
            }
            else {                  
                $_SESSION['address'] = $_POST['address'];
            }

            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính.</div>";
                $error = 0;
            }
            else {                  
                $_SESSION['gender'] = $_POST['gender'];
            }

            if (empty(($_POST["department"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
                $error = 0;
            }
            else {                  
                $_SESSION['department'] = $_POST['department'];
            }
            
            if (empty(($_POST["ngaysinh"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh.</div>";
                $error = 0;
            }
            if (!validateDate($_POST["ngaysinh"]) && !empty($_POST["ngaysinh"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng.</div>";
                $error = 0;
            }
            else {                  
                $_SESSION['ngaysinh'] = $_POST['ngaysinh'];
            }
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $time = date('YmdHis');

            if (!file_exists("upload")) {
                mkdir('upload', 0777, true);
            };

            $target_dir = "upload/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);

            // validate upload image
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                echo "<div style='color: red;'>Dịnh dạng tệp phải là  .JPEG, .PNG hoặc .JPG</div>";
                $error = 0;
            }
            else {
                $filename = $_FILES["image"]["tmp_name"];

                $first_name = pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);

                $image = "upload/" . $first_name . "_" . $time . "." . $extension;
                move_uploaded_file($_FILES["image"]["tmp_name"], './' . $_FILES["image"]["name"]);
                 $_SESSION['image'] = "./" . $_FILES["image"]["name"];

            }


            if($error == 1){
                header("Location: ./thongtin.php");
            }

        }

        function checkInput($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date, $format = 'd/m/Y')
        {
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }
        ?>
        <div class="signup"  style = "border: 2px solid rgb(26, 50, 255);
        background-color: white;
        padding: 50px 40px 50px 50px;
        position: absolute;
        margin-left: 20rem;
        margin-top: 2%;
        width: 50rem;
    ">
            <form action="dangnhap_19000412.php" method="POST" id="form" enctype="multipart/form-data">
                <div class="establish">
                    <label class="box_label" style="flex: 1">
                        Họ và tên
                        <span style="color: red">*</span>
                    </label>
                    <input name="fullName" type="text" class="input-text">
                </div>
                <div class="establish">
                    <label class="box_label" style="flex: 1">
                        Giới tính
                        <span style="color: red">*</span>
                    </label>

                    <div class="gender">
                        <?php
                    for ($i = 0; $i < count($gender); $i++) {
                        echo '
                            <input type="radio" id="'.$i.'" name="gender" value="' . $gender[$i] . '">';
                        echo '
                            <label for="'.$i.'" style="font-size: 18px ;margin: 0 40px 4px 6px ">' . $gender[$i] . '</label> 
                        ';
                    }
                ?>
                    </div>

                </div>
                <div class="establish">
                    <label class="box_label" style="flex: 1">
                    
                        Phân khoa
                        <span style="color: red">*</span>

                    </label>
                    <select name='department'>
                        <?php 
                            foreach ($phankhoa as $key => $value) {
                                echo '<option >' . $value . '</option>';
                            }
                      ?>
                    </select>
                </div>
                <div class="establish">
                   <label class="box_label" style="flex: 1">
                    
                        Ngày sinh
                        <span style="color: red">*</span>

                    </label>
                    <input type="text" name="ngaysinh" id="ngaysinh" class="input-ngaysinh" placeholder="dd/mm/yyyy">
                    <script >
                    $(".ngaysinh").datepicker({
                        format: "dd/mm/yyyy",
                    });
                    </script>
                </div>
                <div class="establish">
                    <label class="box_label" style="flex: 1">
                        Địa chỉ
                        <span style="color: red">*</span>

                    </label>
                    <input type="text" name="address" id="address" class="input-text">
                </div>


                <div class="establish">
                   <label class="box_label" style="flex: 1">
                    
                        Hình ảnh

                    </label>
                    <input type="file" name="image" id="image">
                </div>

                <div class="establish signup-submit">
                    <input type="submit" value="Đăng Kí" name="btnSubmit" style="font-size: 20px;">
                </div>
            </form>
        </div>

    </fieldset>
    </center>
</body>
</html>